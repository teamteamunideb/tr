<?php
    include './php/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>WorkLog</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body style="background-color: #d2d9d4;">
    <?php
    session_start();
        $id = $_SESSION['id'];
        $name = $_SESSION['name'];
        $lastName = $_SESSION['lastName'];
        $isAdmin = $_SESSION['isAdmin'];
        $department = $_SESSION['department'];
        $contactNumber = $_SESSION['contactNumber'];
        $lastTime = $_SESSION['lastTime'];

    ?>
    <div class="container">
        <div class="row" style="margin-top: 5em;">
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
            <div class="col-md-6 col-sm-10 col-xs-10 col-10 border" >
                <p class="marginbottom"><?php echo $name?> <?php echo $lastName?> ID:<?php echo $id?></p> 
                <form action="./php/check.php" method="post">
                    <button type="submit" class="btn btn-block marginbottom" >CHECK-IN/CHECK-OUT</button>
                </form>
                <?php
                    echo $lastTime;
                    date_default_timezone_set("Europe/Riga");
                    $now = date('Y-m-d H:i:s');
                    $currentTime = strtotime($now) - strtotime($lastTime);
                    
                    $minutes = $currentTime /60;
                ?>
                <p class="marginbottom">Current session: <?php echo round($minutes)?> minutes</p>
                <table class="table">
                    <tr>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>

                    <?php
                            $sql = "SELECT * FROM log WHERE userID = $id ORDER BY checkin DESC LIMIT 2";
                            $result = $conn->query($sql); 
                        
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {  
                                
                                $time = isset($row['checkin']) ? $row['checkin'] : '';
                                $status = isset($row['status']) ? $row['status'] : '';

                                ?>
                                    <tr>
                                        <td><?php echo $time?> </td>
                                        <td><?php echo $status?></td>
                                    </tr>
                                <?php

                                }
                            }
                    ?>
                    
                </table>
                <button type="submit" class="btn pull-right" onclick="location.href='index.php'">Logout</button>
                <?php
                if($isAdmin == 1){
                    ?>
                    <button type="submit" class="btnman btn pull-left" onclick="location.href='manager.php'">MANAGER CONTROL</button>
                <?php
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
        </div>
    </div>
</body>
</html>