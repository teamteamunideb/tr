<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>WorkLog</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body style="background-color: #d2d9d4;">
    <div class="container">
        <div class="row" style="margin-top: 5em;">
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
            <div class="col-md-6 col-sm-10 col-xs-10 col-10 border" >
                <form class="marginbottom" action="./php/showEmployee.php" method="post">
                    <input class="search" type="text" name="search" placeholder="Search" aria-label="Search">
                    <button class="btn btnmanager" type="submit">Search</button>
                </form>  
                <button type="submit" class="btn pull-left" onclick="location.href='Employee.php'">Employee</button>
                <button type="submit" class="btn pull-left" onclick="location.href='register.php'">REGISTER</button>
                <button type="submit" class="btn pull-right" onclick="location.href='index.php'">Logout</button>
            </div>
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
        </div>
    </div>
</body>
</html>