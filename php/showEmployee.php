<?php
    include 'connection.php';
    //Data about searchable person
    $id = $_POST["search"];

    $sql = "SELECT id, name, lastName, isAdmin, department, contactNumber, password FROM users WHERE id=$id";
    $result = $conn->query($sql);

    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {  
        $name = isset($row['name']) ? $row['name'] : '';
        $lastName = isset($row['lastName']) ? $row['lastName'] : '';
        $isAdmin = isset($row['isAdmin']) ? $row['isAdmin'] : '';
        $department = isset($row['department']) ? $row['department'] : '';
        $contactNumber = isset($row['contactNumber']) ? $row['contactNumber'] : '';
        $password = isset($row['password']) ? $row['password'] : '';
        }
    }    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <title>WorkLog</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body style="background-color: #d2d9d4;">
    <div class="container">
        <div class="row" style="margin-top: 5em;">
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
            <div class="col-md-6 col-sm-10 col-xs-10 col-10 border" >
                <form class="marginbottom" action="../manager.php" method="post">
                    <button class="btn btn-block" type="submit">Manager Page</button>
                </form>  
                <table class="table ">
                    <tr>
                        <th>Name</th>
                        <th>Dpt</th>
                        <th>ID</th>
                        <th>Contact num.</th>
                        <th>Time spent current month</th>
                    </tr>
                    <tr>
                        <td><?php echo $name?></td>
                        <td><?php echo $department?></td>
                        <td><?php echo $id?></td>
                        <td><?php echo $contactNumber?></td>
                        <td>29 hours 20 min</td>
                    </tr>
                </table>
                <table class="table">
                    <tr>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Time spent</th>
                    </tr>
                    <?php
                        $sql = "SELECT * FROM log WHERE userID = $id ORDER BY checkin DESC LIMIT 30";
                        $result = $conn->query($sql); 

                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {  
                            $status = isset($row['status']) ? $row['status'] : '';    
                            $time = isset($row['checkin']) ? $row['checkin'] : '';
                        ?>
                                    <tr>
                                        <td><?php echo $time?> </td>
                                        <td><?php echo $status?></td>
                                    </tr>
                                <?php
                            }
                        }
                    ?>
                </table>
                <button type="submit" class="btn pull-left" onclick="location.href='../employee.php'">Employee</button>
                <button type="submit" class="btn pull-left" onclick="location.href='../register.php'">REGISTER</button>
                <button type="submit" class="btn pull-right" onclick="location.href='../index.php'">Logout</button>
            </div>
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
        </div>
    </div>
</body>
</html>