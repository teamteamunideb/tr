<?php

include 'connection.php';

$id = $_POST["uname"];

$sql = "SELECT id, name, lastName, isAdmin, department, contactNumber, password FROM users WHERE id=$id";
$result = $conn->query($sql);

    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {  
        $name = isset($row['name']) ? $row['name'] : '';
        $lastName = isset($row['lastName']) ? $row['lastName'] : '';
        $isAdmin = isset($row['isAdmin']) ? $row['isAdmin'] : '';
        $department = isset($row['department']) ? $row['department'] : '';
        $contactNumber = isset($row['contactNumber']) ? $row['contactNumber'] : '';
        $password = isset($row['password']) ? $row['password'] : '';
        }
    }

    if($password != $_POST["psw"]){
        $url='../index.php';
        echo '<META HTTP-EQUIV=REFRESH CONTENT="1; '.$url.'">';
    }else{
        session_start();
        $_SESSION['id'] = $id;
        $_SESSION['name'] = $name;
        $_SESSION['lastName'] = $lastName;
        $_SESSION['isAdmin'] = $isAdmin;
        $_SESSION['department'] = $department;
        $_SESSION['contactNumber'] = $contactNumber;

        $url='../employee.php';
        echo '<META HTTP-EQUIV=REFRESH CONTENT="1; '.$url.'">';
    }

 /* echo $name;
  echo $lastName;
  echo $isAdmin;
  echo $department;
  echo $contactNumber;*/
?>