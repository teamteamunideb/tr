<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>WorkLog</title>
    <!--BOOTSTRAP-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
</head>
<body style="background-color: #d2d9d4;">
    <div class="container">
        <div class="row" style="margin-top: 5em;">
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
            <div class="col-md-6 col-sm-10 col-xs-10 col-10 border" >
                <div class="login">
                        <p>REGISTER</p>
                </div>
                <form action="./php/addUser.php" method="post">
                    <label for="name" class="idpasswordlabel"><b>Name:</b></label>
                    <input type="text" placeholder="Gunta" name="name" required>

                    <label for="lastname" class="idpasswordlabel"><b>Lastname:</b></label>
                    <input type="text" placeholder="Zeile" name="lastname" required>

                    <label for="department" class="idpasswordlabel"><b>Department:</b></label>
                    <input type="text" placeholder="Kossuth ter" name="department" required>

                    <label for="isAdmin" class="idpasswordlabel"><b>isAdmin:</b></label>
                    <input type="text" placeholder="1/0" name="isAdmin" required>

                    <label for="tel" class="idpasswordlabel"><b>Contact number:</b></label>
                    <input type="tel" placeholder="25374739" name="tel" required>
            
                    <label for="psw" class="idpasswordlabel"><b>Password:</b></label>
                    <input type="password" placeholder="********" name="psw" required>
                    <button type="submit" class="btn pull-right" onclick="location.href='employee.html'">Submit</button>
                </form>  
                <button class="btn pull-left" onclick="location.href='../employee.php'">Employee</button>
            </div>
            <div class="col-md-3 col-sm-1 col-xs-1 col-1"></div>
        </div>
    </div>
</body>
</html>